import * as through from 'through2';
import * as csv from 'csvtojson';
import * as path from 'path';
import { DOMParser } from 'xmldom';
import { kml} from '@tmcw/togeojson';

export function deleteLines(numberOfLines = 0) {
  return through.obj(function(file: any, enc: any, cb: any) {
    if (file.isNull()) {
      cb(null, file);
      return;
    }

    if (file.isStream()) {
      cb(new Error('Streaming no supported'));
      return;
    }

    let str = file.contents.toString(enc);
    const newLines = [];

    const lines = str.split(/\r\n|\r|\n/g);

    // tslint:disable-next-line:prefer-for-of
    for (let i = 0; i < lines.length; i++) {

      if (numberOfLines <= 0) {
        // @ts-ignore
        newLines.push(lines[i]);
      }

      numberOfLines--;
    }

    str = newLines.join('\n');
    file.contents = Buffer.from(str);
    // @ts-ignore
    this.push(file);
    cb();
  });
}

export function csvtojson(options: any = {}, isDebug: boolean = false) {
  // tslint:disable-next-line:only-arrow-functions
  return through.obj(async function(file: any, enc: any, cb: any) {
    if (file.isNull()) {
      cb(null, file);
      return;
    }

    if (file.isStream()) {
      cb(new Error('Streaming no supported'));
      return;
    }

    const filePath = path.parse(file.path);
    filePath.base = filePath.base.replace('.csv' ? '.csv' : path.extname(file.path), '.json');
    const jsonArray = await csv(options).fromString(file.contents.toString(enc));
    // @ts-ignore
    file.contents = Buffer.from(JSON.stringify(jsonArray, null, isDebug ? 2 : null));
    file.path = path.format(filePath);
    // @ts-ignore
    this.push(file);
    cb();
  });

}

export function toGeoJson(options: any = {}, isDebug: boolean = false) {
  // tslint:disable-next-line:only-arrow-functions
  return through.obj(async function(file: any, enc: any, cb: any) {
    if (file.isNull()) {
      cb(null, file);
      return;
    }

    if (file.isStream()) {
      cb(new Error('Streaming no supported'));
      return;
    }

    const filePath = path.parse(file.path);
    filePath.base = filePath.base.replace('.kml' ? '.kml' : path.extname(file.path), '.json');

    const domParser = new DOMParser();
    const kmlDoc = domParser.parseFromString(file.contents.toString(enc), 'text/xml');
    const jsonArray = kml(kmlDoc);
    // @ts-ignore
    file.contents = Buffer.from(JSON.stringify(jsonArray, null, isDebug ? 2 : null));
    file.path = path.format(filePath);
    // @ts-ignore
    this.push(file);
    cb();
  });

}

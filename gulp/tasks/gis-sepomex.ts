import { task, src, dest } from 'gulp';
import { Stream } from 'stream';
import { toGeoJson } from '../utils';

function gisSepomex(): Stream {
  return src('data/gis/**/*.kml')
    .pipe(toGeoJson({}, true))
    .pipe(dest('data/converted/gis'));
}

task('gis:sepomex', gisSepomex);

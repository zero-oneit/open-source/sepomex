import { task, src, dest } from 'gulp';
import { Stream } from 'stream';
import * as convertEncoding from 'gulp-convert-encoding';
import { deleteLines, csvtojson } from '../utils';

function importSepomex(): Stream {
  return src('data/**/*.csv')
    .pipe(convertEncoding({from: 'iso-8859-1', to: 'utf8'}))
    .pipe(deleteLines(1))
    .pipe(csvtojson({ delimiter: '|' }))
    .pipe(dest('data/converted'));
}

task('import:sepomex', importSepomex);

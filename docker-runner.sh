#!/usr/bin/env bash

set -e

echo "Waiting for engines to initialize... (60 seconds so far)"
sleep 60

npm start

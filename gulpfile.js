const  path = require('path');
const project = path.join(__dirname, 'gulp/tsconfig.json');

require('ts-node').register({ project });

require('./gulp/gulpfile');
